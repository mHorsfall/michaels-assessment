# Instructions

This is an "open book" assessment.  Feel free to use all the resources you have available to complete the activities to the best of your ability.  If you're unsure how to do something or get stuck, move on.  There's nothing wrong with saying, "I don't know".

To start:

1. Fork this repository to your Bitbucket account.
2. Switch to the introduction branch to start the first activity.
3. Each activity has self-contained instructions and the next steps.